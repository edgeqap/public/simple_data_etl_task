# Simple_Data_ETL_Task

## Task

-  Using Docker Exec and a Bash OR Python Script and AWS CLI
- Create script to be able to accept any arbitrary amount of CSV files.
- e.g docker exec test1.csv test2.csv ...

What it should do:

    - Build docker image from main.py and requirements.txt
    - Push docker image
    - Start EC2 and pull from docker image
    - Run python main.py <args>
    - Return results Ec2 instance which ran python main.py
    
### Desired output:
    
E.g You created a bash script that uses AWS CLI (script.sh)  
Ec2 will start and run main.py on test1.csv and test2.csv    
    
    bash script.sh test1.csv test2.csv
    >> Executing <Handle IOLoop._run_callback(functools.par...0x1165eeac8>>)) created at /Users/Nasrudin/code/Edgeqaap/simple_data_etl_task/.venv/lib/python3.7/site-packages/tornado/platform/asyncio.py:176> took 0.417 seconds
    >> ... Executing <Handle IOLoop.add_future.<locals>.<lambda>(<Future finis...mm/tcp.py:237>) at /Users/Nasrudin/code/Edgeqaap/simple_data_etl_task/.venv/lib/python3.7/site-packages/tornado/ioloop.py:690 created at /Users/Nasrudin/code/Edgeqaap/simple_data_etl_task/.venv/lib/python3.7/site-packages/tornado/concurrent.py:185> took 0.120 seconds
    >> ... Average of double bootstrap results: 6.988799999999999
    
    
Perform the above script 2X, 

- one on Ec2 T2. Nano, 
- one on Ec2 T2.medium

### Desired outcome:
If worked correctly, EC2 T2.Medium will perform faster than Ec2 T2.Nano


Return output results from the above two variations.


- References 
    https://docs.docker.com/engine/reference/commandline/exec/