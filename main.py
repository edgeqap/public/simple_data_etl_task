import argparse

import dask
import numpy as np
import pandas as pd
from dask.distributed import Client, LocalCluster

SAMPLES = 1000
RESAMPLES = 200


def test_read(files):
    dfs = [pd.read_csv(f) for f in files]
    return pd.concat(dfs)


def test_double_monte_carlo(df):
    random_sample = df['test'].sample(frac=1, replace=True)
    resample_output = []
    for i in range(RESAMPLES):
        resample_output.append(random_sample.mean())
    return resample_output


def test_monte_carlo(df):
    lazy_results = []
    for i in range(SAMPLES):
        lazy_result = dask.delayed(test_double_monte_carlo)(df)
        lazy_results.append(lazy_result)
    all_results = dask.compute(*lazy_results)
    return np.mean(all_results)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some csv files.')
    parser.add_argument('csv_files', type=str, nargs='+',
                        help='a csv for the accumulator')
    args = parser.parse_args()
    cluster = LocalCluster()
    client = Client(cluster)
    print(f"Average of double bootstrap results: {test_monte_carlo(test_read(args.csv_files))}")
    client.close()
